== Prologue == 


*[[Lianna]]: We've almost reached the [[The Dragon's Table (location)|Dragon's Table]]. I wonder what [[Validar]] is planning. 

* [[Rowan]]: Me too. I guess we'll find out when we get there! Right, [[Robin]]? 

(Robin lets out a low groan with "..." in subtitles. Robin appears to be in a trance.) 

*[[Chrom]]: Robin, are you Ok? 
*Robin:(Weakly)Y-yeah... 
*Rowan:Something's wrong. What's the matter? 
*Robin:I just haven't been able to sleep well latley... Augh! My head!
*Lianna:You don't look well at all! You probably need rest. 
*Robin:You're right... I should probably sit out the next battle.
*Chrom:You're in no shape to protect yourself. I'll stay back and keep an eye on you. 
*Robin:Thanks, Chrom... 

(Chrom and Robin head somewhere to rest.) 

*[[Lucina|Marth]]:Hmm. 
*Lianna:We won't have to worry with Chrom watching over you! 
*Rowan:You can leave the Dragon's Table to us! 
Marth:Actually, I've got something that needs my attention. 
*Rowan:What?! You aren't going to fight with us? 
*Marth:I'm sorry---I'll catch up with you later.  
*[[Darios]]: Hrmph... Very well. Come on, everyone! Prepare yourselves for the battle ahead! 

== Robin Attacks ==

* Chrom:Robin? What's wrong with you? Could...could he be possessed?

== After Knocking Sense Back Into Robin ==

* Chrom:Robin! Are you Ok? 
* Robin;Chrom? Where am I? The last thing I remember is entering the forest... 
* Chrom:Thank the gods. You've come out of it. Rest now... We can speak later. 
* Robin:I don't want to cause any trouble for you, Chrom. I think I will stay here and rest.
