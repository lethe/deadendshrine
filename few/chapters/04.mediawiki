== Opening ==
* '''[[Rowan]]''': Where do we start looking for heroes?

* '''[[Lianna]]''': That reminds me . . . A mysterious fighter joined in a battle we fought earlier. He helped us against some Outrealm Fiends in town. 

* '''Rowan''': What was his name? He could help us!

* '''[[Darios]]''': If I recall correctly, he said his name was Marth. 

* '''[[Chrom]]''': Marth?! You can't have met the legendary Hero-King Marth . . . He lived thousands of years ago. There's just no way.

* '''[[Robin]]''': It's a better lead than wandering around, asking everyone, "Have you seen a hero?"

* '''[[Cordelia]]''': In that case, I will go scout ahead!

''(Cordelia leaves and returns)''
* '''Cordelia''': Chrom! I was able to gather some valuable information in the desert!

* '''Lianna''': What, already?! That's amazing! You really can do everything . . .

* '''Chrom''': Cordelia is the best of the best. We'd be lost without her.

* '''Cordelia''': I-uh! Uh . . . huh . . .

* '''Chrom''': She's invaluable on the battlefield. Truly, I consider her irreplaceable. 

* '''Chrom''': . . . Cordelia, what's wrong?

* '''Cordelia''': Ehem! Ah, well . . . forgive me, Chrom! Allow me to continue my report. Apparently, the Gristonne army in the desert recently hired a skilled mercenary. He goes by "Marth", wears a mask, and uses unheard-of sword techniques. 

* '''Rowan''': It must be the same person!

* '''Chrom''': It couldn't be the Hero-King himself . . . But I suppose we should investigate. 
* '''Darios''': The Gristonne army blocks our way again. Could my father really be behind this?
* '''Lianna''': Darios . . . 
* '''Darios''': We've got to find out what's going on . . . Come, let's move!

== Battle ==
* '''Cordelia''': This sandstorm seems impassable . . . At this rate, we'll never get through!
* '''Robin''': The sandstorm is keeping to a specific location . . . Could this be magic?
* '''Cordelia''': Magic . . . I did spot an odd stone pillar! . . . Could breaking it affect the storm?
* '''Frederick''': We won't be able to reach all the pillars unless we take control of the forts. 
* '''Robin''': Ngh! My injury's acting up again . . . Perhaps I should stick to the  rear.
* '''Cordelia''': Of course. I'll escort you an keep an eye out for any danger.
* '''Rowan''': Is the pillar Cordelia mentioned? It is kinda sketchy, so . . . Let's wreck it!
* '''Lianna''': Nothing's happening . . . Try breaking the other pillar!
* '''[[Fighter]]''': That's the woman that came scouting earlier! Don't let her escape!
* '''Chrom''': We have to do something!
* 
[[Category:Game Script]]
[[Category:Fire Emblem Warriors Scripts]]
