{{Unfin}}

==Prologue==
* '''[[Rowan]]:''' I can't believe Cordelia didn't come with us. I wanted a ride on her pegasus.
* '''[[Frederick]]:''' Oh, you don't know? Pegasuses tend to dislike men, so it's extremely rare for a man to ride one.
* '''Rowan''': What? But that means-- Will I never become a [[Pegasus Knight|pegasus knight]]?!
* '''Frederick:''' It is... unlikely, but isn't it enough to ride into battle on horseback?
* '''Rowan:''' Sure, but... I'm not very good at riding on horseback.
* '''Frederick:''' That skill is even more important when you are on a pegasus.
* '''Rowan:''' It's my biggest barrier to beoming a knight. (noticing something offscreen) Wait, who's that?
''(The scene transitions to the youngest Hoshidan princess, Sakura, who is being attacked by some ruffians.)''
* '''[[Sakura]]:''' (gasps) I've been spotted! What should I do?!
''(The ruffians attack Sakura, who gasps from being attacked. Rowan and Chrom take out the ruffians to save her.)''
* '''Rowan:''' Are you okay?
* '''Sakura:''' Oh, thank you. You saved me.
* '''Rowan:''' Looks like you'll be fine. That's a relief.
* '''Chrom:''' There may still be enemies nearby. You'd better stick with us for now.
* '''Sakura:''' Oh, okay. Thank you. Um, I don't know your names.
* '''Robin:''' Heads up, everyone. Something's flying towards us!
''(The scene transitions to the second eldest Hoshidan princess, Hinoka, who thinks Rowan's group is kidnapping her.)''
* '''[[Hinoka]]:''' Sakura, are you all right? Don't worry; I'll have you free in no time! (to the other characters in the group) So bold as to try kidnapping my sister in broad daylight?! Your lives end here!
''(The scene transitions back to Rowan's group)''
* '''Rowan:''' No, wait! You've got it all wrong; we were helping!
* '''Robin:''' Doesn't look like she's in the mood to talk! We'd etter defend ourselves!

==Movie - Showdown With Hinoka==
''(Hinoka comes riding in one her pegasus. She starts spinning her naginata above her head like a helicopter blade.)''
* '''Hinoka:''' You dare kidnap a member or the royal family of Hoshido?!
''(She does air slashes, and then points her naginata at Rowan. A subtitle saying "Princess of Hoshido - Hinoka" appears.)''
* '''Hinoka:''' Your punishment is death!
''(As the camera pans behind her showing Rowan and Sakura from a distance, the scene fades out.)''

==Battle==
[[Category:Fire Emblem Warriors Scripts]]
