/*
Tor2web blocking script
written by Vane Vander <https://mayvaneday.org>
released under MIT License
*/

let text = window.location.href;
let pet = text.includes("onion.pet");
let ws = text.includes("onion.ws");
let search = text.includes("onionsearchengine.com");
let cyber = text.includes("cyber-hub.pw");
let ly = text.includes("onion.ly");
let moe = text.includes("onion.moe");
let web = text.includes("darknet2web.com");
let web2 = text.includes("darknet2web.info");
let gate = text.includes("torgateway.com");
let i2p = text.includes("i2p.mk16.de");
let i2p2 = text.includes("i2phides.me");
let nl = text.includes("tor2web.nl");
let re = text.includes("onion.re");
let foundation = text.includes("onion.foundation");
let india = text.includes("torproxy.in");
let nz = text.includes("onion.nz");
let autos = text.includes("onion.autos");
let dnshook = text.includes("dnshook.site");
let torify = text.includes("torify.net");
if ((pet === true) || (nl === true) || (autos == true) || (i2p === true) || (ws === true) || (gate === true) || (search === true) || (foundation === true) || (web2 === true) || (i2p2 === true) || (re === true) || (cyber === true) || (ly === true) || (web === true) || (nz === true) || (india === true ) || (moe === true)) || (dnshook === true) || (torify === true) {
    window.location.replace("https://theannoyingsite.com");
}
