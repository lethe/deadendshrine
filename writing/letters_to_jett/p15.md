People come and go in my life, and the surfaces of my hands are no different. On my right thumb used to be a vertical scar on the joint that, in my childhood, would part like the beaches of a crimson ocean every winter and then close again come spring. Reacting to the cue of the temperature dropping, like a sliver in the heart to announce the arrival of the indifferent sky and the seasonal purposelessness that came with it. A sliver I would yearn to curl up into, the only color in the grayscale expanse of the season. It wouldn't usually bleed, but it *would* ache in response to touch. But sometime in my adolesence it stopped appearing, forgot to pick up a ticket for the winter train, decided that the yearly trip to my reality was no longer worth it.

*Did I do something to repel you?*

*Did I do something?*

*Did I...*

"Lethe?"

I don't look at her. I don't break gaze with the new scars across my knuckles. Each a few millimeters long, stinging in their own way when cleaned with soap. But none could ever hope to compete with the childhood thumb scar. Their mother. Their progenitor. The one who died to create a world they could live in.

"*Lethe?*"

A tassel from my new cloak brushes my hands. For all the months Jett had been working as a seamstress, I'd never been able to find her studio, never been able to visit her. But for my mother's birthday, my grandmother took us both to one in this Inside. She bought us both cloaks, woven on loom by disabled adults looking for dignity in their lives. She wrapped me in one of midnight blue, starlight with the Rainbow Bridge running through it. I looked at the handwritten price tag. I locked eyes with the one who'd made the cloak, who was clacking along on her loom at that moment, who would be for as long as she could see herself in the future.

Did she see any of herself in me? Did she wonder what life would be like if she were independent enough to live on her own? Because I saw myself in her. Saw myself in a future where my disability gets worse and my savings run out and I can't take care of myself anymore and I lose everything I've worked so hard to achieve these past six months. I saw myself in her, and I was spooked into silence for the rest of the trip.

"*Lethe!*"

Jett sits down beside me, forces open my now-clenched hands. I can see healing papercuts on hers. A new job at the library. Stopgap or new career path, I can't tell.

"Lethe. Breathe. Speak to me."

"I... um..." I wiggle my fingers, one after the other. One is redder than the others, more tender, more sore. "When you worked at the hospital, did you ever have to deal with [burn patients](https://web.archive.org/web/20240913172048/https://www.ncbi.nlm.nih.gov/books/NBK513287/)?"

"You mean, back at the Town? Not personally."

I show her my finger. "So you *can't* tell me how much of my body I burned today?"

"Less than one percent." She grabs my arm, lifts it up. "This is nine percent." She drops that arm, grabs the other one, lifts that one. "This is also nine percent." She lets go, pats one of my legs. "Eighteen." Other leg. "Eighteen." Pats the top of my head. "Nine." Looks me right in the eyes as she puts a hand on one of my breasts. "Front of your chest is eighteen." Other hand on my back between my wings. "Back is eighteen."

"Your math is a little off."

"That's *my* line," she growls. Her hands drop. "The *other* one percent-" her fingers start walking along my leg, towards my hips- "belongs to..."

Then less than one percent of her body is in mine.

***

We're lying on the floor, side-by-side. One of her arms is thrown over her eyes to block out the crack of sun spilling out from between the curtains. My childhood would have thought it a rare sight, bright sun in the middle of winter, but it's almost the end of December and we've had no persistent snow in Minnesota. No snow banks, no snowpeople, no snowball fights.

Without getting up, I feel along the floor for her free hand, grab it, start curling her fingers in and out as I count.

"Hmm?"

"Jett? When you take a number with at least two digits in it and add the digits together, and then you add nine, and then you add the digits of that answer together, you get the same thing as if you hadn't added nine in the first place."

She lifts her arm a bit, side-eyes me. "So this whole time I was thinking about us curling up under your mother's quilt, you were doing complex calculus in your head."

"Actually, I failed calculus in college."

"Yeah. I know. We coitus-ed the morning of your finals. And then you had an existental crisis, answered two questions on your final exam, and then booked it off campus as fast as your sprained knee would let you."

"And... there was a quilt from my mother hanging in that room, too."

"Very perceptive, Lethe." She wiggles her hand free from my grasp. "So thirty-five. Three plus five is... eight. Plus nine makes seventeen. Seven plus one makes... eight." Her brow furrows. "No, that has to be wrong. Sixty-five. Six plus five make eleven. One and one make two. Plus nine is... eleven again." She shakes her head. "I'm missing something. My angel number. One and one and one and four make seven. Plus nine is sixteen. One and six are- *dammit!*"

"Difficulties?"

"It doesn't feel like it should be true. But it is. You're going to drive me insane long before we ever get to Sablade."

"Speaking of that..." I pull myself up to my knees, loom over Jett, who is still counting digits on her fingers. "You told me to study Asatru a few years ago. And they really like the number nine. Like, *really* like nine. There are nine worlds. And we're in Midgard, right in the middle of everything. But that doesn't make any sense to me, Jett, because I *know* there are infinite worlds in the Outside." I touch my chest. "*I'm* one of them! So does everything I know, everything I've experienced, just neatly fit into Midgard because it's clearly nowhere else? I'm absolutely confounded. Explain this to me."

"I mostly just wanted you to study the runes." She pulls herself up to sitting. I straighten myself to give her room. "The map of the nine worlds isn't intended to be literal. Even if this one Inside was Midgard, where do you put the other planets? If life was licked out of the brine by Audhumla, then where did aliens come from? Do Huginn and Muninn put on space helmets to go visit the other planets to tell Odin what happens there? You'd think, with his desperate hunger for knowledge, he'd have more efficient methods. It's mythology, Lethe. It doesn't have to hold up to scrutiny. It's just a pretty story intended as a cultural baseline."

"I... wonder what myths the people of Sablade will write about us." A pause. "I hope that they write that we stay in love with each other forever."

Jett starts counting on her fingers again.

"Lethe, guess what else has the number nine?"

"What?"

"What important event happened in 2014?"

"I... discovered I like women? And I had my first relationship in this Inside? And my first breakup?"

She winces. "*And then what?* What other thing happened for the first time in this Inside?"

*Your powers are perfectly matched here.*

*Your powers are... here.*

*You're... here!*

"I... I saw your face!"