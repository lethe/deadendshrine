## Lyke (body)

- physical body itself

## Hyde (appearance of body)

- gives shape/form to the person
- mannerisms and self-presentation
- actors change this a lot when acting

## Athem (breath)

- breath of life / life force
- keeps lyke and hyde alive
- leaves body when body dies

## Hugh ([left side of brain](https://www.medicalnewstoday.com/articles/321037))

- symbolised by raven Huginn
- analytical, mathematical

## Myne (memory)

- symbolised by raven Muninn
- reflects and imagines
- communicates through images and dreams
- governs dreams and memories
- communicates with collective unconsciousness

## Wode (inspiration)

- condition of altered consciousness, like divine inspiration
- you need to be in this state to work effective magic
- works best when hugh and myne are cooperating together

## Fetch

- surrounds you, but isn't necessarily a part of *you* yourself

three possible forms:

1. an animal
2. a person of the sex you are attracted to
3. a geometric shape

- stays with you your whole life
- similar to a guardian angel, but not exactly the same

## Hamingja (luck)

## Soul

- shade of the deceased
- goes to one of the worlds of the undead (Valhalla, Hel, etc)

***

sources:

- *Northern Magic* by Edred Thorsson
- *Apophis* by Michael Kelly